import socket
import common
import pickle

if __name__=="__main__":
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    listen_socket.bind(("0.0.0.0", common.TARGET_UDP_BROADCAST_PORT))
    while True:
        data, addr = listen_socket.recvfrom(1024) # buffer size is 1024 bytes
        beacon_message = pickle.loads(data)
        print(beacon_message)
