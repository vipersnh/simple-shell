from collections import namedtuple
import socket
import time

TARGET_TCP_PORT = 1111
TARGET_UDP_BROADCAST_PORT = 2222

beacon_t = namedtuple("beacon_t", ["ip_address", "port_number", "machine_name", "unique_id", "is_connected"])

command_t = namedtuple("command_t", ["cmd_list"])

acknowledge_t = namedtuple("acknowledge_t", ["stdout", "stderr", "return_code"])

def send_message(sock, message):
    length = len(message)
    byte1 = (length & 0xFF)
    length >>= 8
    byte0 = (length & 0xFF)
    length >>= 8
    assert (length==0)
    sock.send(chr(byte0))
    sock.send(chr(byte1))
    sock.send(message)

def recv_message(sock):
    message = None
    def recv_data(sock, length):
        data = None
        while True:
            try:
                data = sock.recv(length)
                if len(data)==0:
                    data = None
                break
            except socket.error:
                time.sleep(0.1)
        return data
    byte0 = recv_data(sock, 1)
    byte1 = recv_data(sock, 1)
    if byte0==None or len(byte0)==0:
        return None
    if byte1==None or len(byte1)==0:
        return None
    length = (ord(byte0) << 8) | ord(byte1)
    data = recv_data(sock, length)
    if data==None or len(data)==0:
        return None
    else:
        return data
 
