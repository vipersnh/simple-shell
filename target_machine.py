import threading
import pdb
import signal, os
import time
import socket
import common
import pickle
import subprocess

stop_program = False
target_connected = False

def handler(signum, frame):
    global stop_program
    print 'Signal handler called with signal', signum
    stop_program = True

TARGET_IP_ADDR = "127.0.0.1"
TARGET_MACHINE_NAME = "some-name"
TARGET_UNIQUE_ID = 0x121212

def start_udp_messenger_process(ip_addr, broadcast_port, command_port, machine_name, unique_id):
    print("start_udp_messenger_process: Called with {0}, {1}, {2}, {3}".format(ip_addr, broadcast_port, machine_name, unique_id))
    broadcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    broadcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    broadcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    global stop_program
    global target_connected
    while stop_program==False:
        beacon_message = common.beacon_t(ip_address=ip_addr, port_number=command_port, machine_name=machine_name, unique_id=unique_id, is_connected=target_connected)
        beacon_message_str = pickle.dumps(beacon_message)
        broadcast_socket.sendto(beacon_message_str, ('255.255.255.255', broadcast_port))
        time.sleep(1)
        

def start_tcp_command_handler_process(ip_addr, port):
    print("start_tcp_command_handler_process: Called with {0}, {1}".format(ip_addr, port))
    global target_connected
    global stop_program
    command_socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    command_socket_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    command_socket_server.bind(("0.0.0.0", port))
    command_socket_server.settimeout(1)
    command_socket_server.listen(1)
    while stop_program==False:
        target_connected = False
        try:
            # Wait for a connection on the server port
            (command_socket, address) = command_socket_server.accept()
            target_connected = True
        except socket.timeout:
            pass
        if target_connected:
            print("Target accepted")
            while True:
                command_message = common.recv_message(command_socket)
                if command_message:
                    command = pickle.loads(command_message)
                    print("executing command: {0}".format(' '.join(command.cmd_list)))
                    process = subprocess.Popen(command.cmd_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                    (out, err) = process.communicate()
                    ret = process.returncode
                    print("Command Executed: stdout = {0}, stderr = {1}, returncode = {2}".format(out, err, ret))
                    acknowledge = common.acknowledge_t(stdout=out, stderr=err, return_code=ret)
                    acknowledge_message = pickle.dumps(acknowledge)
                    common.send_message(command_socket, acknowledge_message)
                else:
                    break


if __name__=="__main__":
    signal.signal(signal.SIGINT, handler)
    udp_task = threading.Thread(target=start_udp_messenger_process, args=(TARGET_IP_ADDR, common.TARGET_UDP_BROADCAST_PORT, common.TARGET_TCP_PORT, TARGET_MACHINE_NAME, TARGET_UNIQUE_ID,))
    tcp_task = threading.Thread(target=start_tcp_command_handler_process, args=(TARGET_IP_ADDR, common.TARGET_TCP_PORT,))
    udp_task.start()
    tcp_task.start()
    while stop_program==False:
        time.sleep(1)
    udp_task.join()
    tcp_task.join()
    print("Completed session!")
