import time
import common
import sys
import os
import pickle
import socket
import pickle

if __name__=="__main__":
    if len(sys.argv) != 2:
        print("Please execute with ip-addr of the target to connect!")
        sys.exit(0)
    ip_address = sys.argv[1]
    print("Connecting to target : {0}".format(ip_address))

    target_found = False
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    listen_socket.bind(("0.0.0.0", common.TARGET_UDP_BROADCAST_PORT))
    beacon_message = None
    while True:
        data, addr = listen_socket.recvfrom(1024) # buffer size is 1024 bytes
        beacon_message = pickle.loads(data)
        if beacon_message.ip_address==ip_address:
            target_found = True
            break
        else:
            print('\t.')
            time.sleep(1)
        
    control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    control_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    control_socket.connect((ip_address, beacon_message.port_number))
    print("Connected to target!!")
    while True:
        command = raw_input("remote-terminal: ")
        try:
            command_list = eval(command)
        except:
            print("invalid commands list, please correct it")
            continue
        command = common.command_t(cmd_list=command_list)
        command_message = pickle.dumps(command)
        common.send_message(control_socket, command_message)
        ack_message = common.recv_message(control_socket)
        ack = pickle.loads(ack_message)
        print("Received: {0}".format(ack))

